<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'home',

                'defend/place' => 'defend/place',

                'process/<processTypeId>' => 'process',
                'defend/<defend_type_id>' => 'defend',


                '<controller>/<action>' => '<controller>/<action>',

                'process-step/advisor-step1/<processTypeId>/<step>' => 'process-step/advisor-step1',
                'process-step/validation/<processId>/<processStepSequence>' => 'process-step/validation',
                'process-step/<processId>/<processStepSequence>' => 'process-step',

                'dummy-person/login/<user_id>' => 'dummy-person/login',
                'dummy-person/person-api/<user_id>' => 'dummy-person/person-api',
                'dummy-petition/complete/<petition_type_id>' => 'dummy-petition/complete',

                'api/advisor/<student_id>' => 'api/advisor',
                'somebody-api/person/<person_id>' => 'somebody-api/person',
                'somebody-api/petition/<studentId>/<petitionTypeId>' => 'somebody-api/petition',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
