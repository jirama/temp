<?php

namespace app\controllers;

use app\models\Project;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class SessionController extends Controller
{
    public function actionSetAdditional()
    {
        $request = Yii::$app->request;
        foreach ($request->post() as $key => $value) {
            $session = Yii::$app->session;
            if (!isset($session['additional'])) {
                $session['additional'] = [];
            }
            $additional = $session['additional'];
            $additional[$key] = $value;
            $session['additional'] = $additional;
            return $value;
        }
        return null;
    }

    public function actionGetAdditional()
    {
        $session = Yii::$app->session;
        $additional = $session['additional'];
        return Json::encode($additional);
    }
}
