<?php

namespace app\controllers;

use app\models\Project;
use Yii;
use yii\helpers\Url;

class Validation extends Validation_
{
    protected $session;

    function __construct()
    {
        $this->session = Yii::$app->session;
    }

    public function advisor_petition_complete()
    {
        $student_id = $this->session->get('id');
        $petition_type_id = 0;
        $url = Url::home(true) . 'somebody-api/petition/' . $student_id . '/' . $petition_type_id;
        $path = '$..petition_status_id';
        $should_be = 1;
        $json = $this->get_json($url);
        $data = null;
        if ($json !== '[null]')
            $data = $this->get_data($json, $path);
        return $data === $should_be;
    }

    public function project_not_empty()
    {
        $student_id = $this->session->get('id');
        $project = Project::find()->where(['student_id' => $student_id])->one();
        return !empty($project);
    }

    public function proposal_petition_complete()
    {
        return false;
    }

    public function proposal_committee_petition_complete()
    {
        return false;
    }
}