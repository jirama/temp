<?php

namespace app\controllers;

use app\controllers\JsonPath\JsonStore;

class Validation_
{
    protected function get_json($url)
    {
        return file_get_contents($url);
    }

    protected function get_data($json, $path)
    {
        $json = new JsonStore($json);
        if ($json !== null) {
            $data = $json->get($path);
            if ($data[0] !== null) {
                return $data[0];
            }
            return null;
        }
        return null;
    }
}
