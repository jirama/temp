<?php

namespace app\controllers;

use app\models\Project;
use Yii;
use app\models\DummyPetition;
use app\models\Process;
use yii\helpers\Json;
use yii\web\Controller;

class TestController extends Controller
{
    public function actionAutoAddProject()
    {
        $session = Yii::$app->session;
        $student_id = $session->get('id');
        $project = Project::find()->where(['student_id' => $student_id])->one();
        if (empty($project)) {
            $project = new Project();
            $project->student_id = $student_id;
            $project->project_name_th = 'ระบบการจัดการบัณฑิตศึกษา';
            $project->project_name_eng = 'Graduate Student Management System';
            $project->save();
        }
        return $this->redirect(Yii::$app->homeUrl . 'process');
    }

    public function actionIndex()
    {
        $project['project_name_th'] = 'th';
        $project['project_name_eng'] = 'eng';
        return Json::encode($project);
//        foreach (Yii::$app->request->post() as $key => $value) {
//            if ($key == 'value') {
//                echo $value;
//            }
//        }




//        $action = new Action();
//        $action->insert_advisor_from_petition();
////        print_r($rules);
////        foreach ($rules as $rule) {
////            if ($rules == ){
////
////            }
////        }
////        $string = 'process_step_status';
////
////        $index = strpos($string, '_');
////        $index_ = $index + 1;
////        $lc = substr($string, $index_, 1);
////        $front = substr($string, 0, $index - 1);
////        $middle = strtoupper($lc);
////        $end = substr($string, $index_, 1);
////        echo $front.$middle.$end;
//////        substr_replace();
////
////
////        $className = 'app\models\Advisor';
////        $model = Yii::createObject([
////            'class' => $className,
////        ]);
////        $rule = $model->rules();
////        print_r($rule[0][0]);
    }

    public function actionAdvisorPetitionSuccess()
    {
        $session = Yii::$app->session;
        $student_id = $session->get('id');
        $petitionTemp = DummyPetition::find()->where(['student_id' => $student_id, 'petition_type_id' => 0])->one();
        $petition = DummyPetition::findOne($petitionTemp->petition_id);
        $petition->petition_status_id = 1;
        $petition->save();
        $this->redirect(Yii::$app->homeUrl . 'process/advisor');
    }

    public function actionProcess($processTypeId, $step)
    {
        $process = Process::find()->where(['student_id' => 'student0'])->andWhere(['process_type_id' => $processTypeId])->one();
        $data['process'] = $process;
        $data['step'] = $step;
        return $this->render('process', $data);
    }
}
