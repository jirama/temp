<?php

namespace app\controllers;

use app\models\DummyPerson;
use app\models\DummyPersonType;
use app\models\DummyPetition;
use app\models\DummyPetitionComponent;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;

class DummyPetitionController extends Controller
{
    public function actionInsertAdvisor()
    {
        $session = Yii::$app->session;
        $studentId = $session->get('id');
        $petition = DummyPetition::find()->where(['student_id' => $studentId, 'petition_type_id' => 0, 'petition_active' => 1])->one();
        if (empty($petition)) {
            $petition = new DummyPetition();
            $petition->petition_active = 1;
            $petition->petition_type_id = 0;
            $petition->student_id = $studentId;
            $petition->petition_status_id = 2;
            $petition->save();
            $petitionId = $petition->petition_id;
            $this->insertAdvisorComponent($studentId, $petitionId);
        }
        $this->redirect(Yii::$app->homeUrl . 'process/advisor');
    }

    private function insertAdvisorComponent($studentId, $petitionId)
    {
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'student_id';
        $petitionComponent->petition_component_value = $studentId;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_1';
        $petitionComponent->petition_component_value = 'teacher1';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_2';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_3';
        $petitionComponent->petition_component_value = null;

        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_4';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_5';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
    }

    public function actionTest()
    {
//        $request = Yii::$app->request;
//        $request->post('project-name-th');
//        echo '55555';
        $session = Yii::$app->session;
        echo $session->getFlash('project-name-th');
    }

    public function actionInsertProposal()
    {
        $session = Yii::$app->session;
        $studentId = $session->get('id');
        $session->setFlash('project-name-th', '55555555');
        $this->redirect(Yii::$app->homeUrl . 'dummy-petition/test');
        $petition = DummyPetition::find()->where(['student_id' => $studentId, 'petition_type_id' => 1, 'petition_active' => 1])->one();
        if (empty($petition)) {
            $petition = new DummyPetition();
            $petition->petition_active = 1;
            $petition->petition_type_id = 1;
            $petition->student_id = $studentId;
            $petition->petition_status_id = 2;
            $petition->save();
        $petitionId = $petition->petition_id;
        $this->insertProposalComponent($studentId, $petitionId);
        }
        $this->redirect(Yii::$app->homeUrl . 'process/proposal');
    }

    private function insertProposalComponent($studentId, $petitionId)
    {
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'student_id';
        $petitionComponent->petition_component_value = $studentId;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'project_name_th';
        $petitionComponent->petition_component_value = 'ระบบการจัดการบัณฑิตศึกษา';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'project_name_eng';
        $petitionComponent->petition_component_value = 'Graduate Student Management System';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
    }

    public function actionInsertProposalCommittee()
    {
        $session = Yii::$app->session;
        $studentId = $session->get('id');
        $petition = DummyPetition::find()->where(['student_id' => $studentId, 'petition_type_id' => 2, 'petition_active' => 1])->one();
        if (empty($petition)) {
            $petition = new DummyPetition();
            $petition->petition_active = 1;
            $petition->petition_type_id = 2;
            $petition->student_id = $studentId;
            $petition->petition_status_id = 2;
            $petition->save();
            $petitionId = $petition->petition_id;
            $this->insertProposalCommitteeComponent($studentId, $petitionId);
        }
        $this->redirect(Yii::$app->homeUrl . 'process/proposal');
    }

    private function insertProposalCommitteeComponent($studentId, $petitionId)
    {
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'student_id';
        $petitionComponent->petition_component_value = $studentId;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'date';
        $petitionComponent->petition_component_value = date('d');
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'month';
        $petitionComponent->petition_component_value = date('m');
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'year';
        $petitionComponent->petition_component_value = date('Y');
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'time';
        $petitionComponent->petition_component_value = '10:00';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'place';
        $petitionComponent->petition_component_value = 0;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_11';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_12';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_21';
        $petitionComponent->petition_component_value = 'teacher2';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_22';
        $petitionComponent->petition_component_value = 'teacher3';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_31';
        $petitionComponent->petition_component_value = 'teacher1';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_41';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_42';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_43';
        $petitionComponent->petition_component_value = null;
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
        $petitionComponent = new DummyPetitionComponent();
        $petitionComponent->petition_component_name = 'teacher_id_00';
        $petitionComponent->petition_component_value = 'teacher2';
        $petitionComponent->petition_id = $petitionId;
        $petitionComponent->save();
    }

    public function actionComplete($petition_type_id)
    {
        $session = Yii::$app->session;
        $student_id = $session->get('id');
        $petitionTemp = DummyPetition::find()->where(['student_id' => $student_id, 'petition_type_id' => $petition_type_id, 'petition_active' => 1])->one();
        $petition = DummyPetition::findOne($petitionTemp->petition_id);
        $petition->petition_status_id = 1;
        $petition->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

}