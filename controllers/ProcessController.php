<?php

namespace app\controllers;

use app\models\Process;
use app\models\ProcessStep;
use app\models\ProcessStepTemplate;
use app\models\ProcessStepValidation;
use app\models\Project;
use app\models\Semester;
use Yii;
use yii\web\Controller;

class ProcessController extends Controller
{

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $session = Yii::$app->session;

        if ($session->get('id') == null) {
            exit('<body style="background-color: black"><h1 style="text-align: center;color: white;margin-top: 21%">
                    <span style="margin-right: 100px">" PLEASE LOGIN "</span><br>
                    <span style="margin-left: 250px">- JAY 2017<span><br>
                    <a style="text-decoration: none;color:red" href="' . Yii::$app->homeUrl . '">GO HOME</a>
                    </h1></body>');
        } else if ($session->get('type') != 0) {
            exit('<body style="background-color: black"><h1 style="text-align: center;color: white;margin-top: 21%">
                    <span style="margin-right: 100px">" YOU ARE NOT STUDENT "</span><br>
                    <span style="margin-left: 250px">- JAY 2017<span><br>
                    <a style="text-decoration: none;color:red" href="' . Yii::$app->homeUrl . '">GO HOME</a>
                    </h1></body>');
        }
    }

    public function actionProcessStep($processId, $processStepSequence)
    {
        $processStep = ProcessStep::find()->where(['process_id' => $processId, 'process_step_sequence' => $processStepSequence])->one();
        $data['processStep'] = $processStep;
        echo Yii::$app->view->renderFile(Yii::$app->viewPath . '/process/templates/' . $processStep->process->process_type_id . '/step' . $processStepSequence . '.php', $data);
    }

    public function actionIndex($processTypeId = null)
    {
        if ($processTypeId == null) {
            $session = Yii::$app->session;
            $student_id = $session->get('id');
            $project = Project::find()->where(['student_id'=>$student_id])->one();
            $data['project'] = $project;
            return $this->render('index', $data);
        } else {
            while (true) {
                $process = $this->getProcess($processTypeId);
                if (!empty($process)) {
                    $data['process'] = $process;
                    return $this->render('templates/index', $data);
                }
            }
            return null;
        }
    }

    private function getProcess($processTypeId)
    {
        $session = Yii::$app->session;
        $studentId = $session->get('id');
        $process = Process::find()->where(['student_id' => $studentId, 'process_type_id' => $processTypeId])->one();
        if (empty($process)) {
            $this->insertProcess($studentId, $processTypeId);
        }
        return $process;
    }

    private function insertProcess($studentId, $processTypeId)
    {
        $process = new Process();
        $process->student_id = $studentId;
        $process->process_type_id = $processTypeId;
        $semester = Semester::find()->where(['semester_active' => 1])->one();
        $semesterId = $semester->semester_id;
        $process->semester_id = $semesterId;
        $process->save();
        $processId = $process->getPrimaryKey();
        $processStepTemplates = ProcessStepTemplate::find()->where(['process_type_id' => $processTypeId])->all();
        $processStepStatusId = 1;
        foreach ($processStepTemplates as $processStepTemplate) {
            $processStep = new ProcessStep();
            $processStep->process_step_sequence = $processStepTemplate['process_step_template_sequence'];
            $processStep->process_step_name = $processStepTemplate['process_step_template_name'];
            $processStep->process_step_status_id = $processStepStatusId;
            $processStep->process_id = $processId;
            $processStep->save();
            $processStepStatusId = 0;
        }
    }
}