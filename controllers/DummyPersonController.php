<?php

namespace app\controllers;

use app\models\DummyPerson;
use app\models\DummyPersonType;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;

class DummyPersonController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function actionPersonApi($user_id = null)
    {
        if ($user_id == null) {
            $persons = DummyPerson::find()->all();
            return Json::encode($persons);
        }
        $person = DummyPerson::findOne($user_id);
        return Json::encode($person);
    }

    public function actionLogin($user_id)
    {
        $data = file_get_contents(Url::home(true) . 'dummy-person/person-api/' . $user_id);
        $person = Json::decode($data);
        echo $person['person_first_name'];

        $session = Yii::$app->session;
        $session->set('id', $person['person_id']);
        $session->set('name', $person['person_prefix'] . ' ' . $person['person_first_name'] . ' ' . $person['person_last_name']);
        $session->set('type', $person['person_type_id']);
        $this->redirect(Url::home(true));
    }

    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->removeAll();
        $this->redirect(Url::home(true));
    }
}