<?php

namespace app\controllers;

use app\models\Process;
use app\models\ProcessStep;
use app\models\ProcessStepActionRequire;
use app\models\ProcessStepTemplate;
use app\models\ProcessStepValidationRequire;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;

class ProcessStepController extends Controller
{
    public function actionIndex($processId, $processStepSequence)
    {
        $processStep = ProcessStep::find()->where(['process_id' => $processId, 'process_step_sequence' => $processStepSequence])->one();
        $data['processStep'] = $processStep;
        echo Yii::$app->view->renderFile(Yii::$app->viewPath . '/process/templates/' . $processStep->process->process_type_id . '/step' . $processStepSequence . '.php', $data);
    }

    public function actionValidation()
    {
        $request = Yii::$app->request;
        $process_id = $request->post('process_id');
        $process = Process::findOne($process_id);
        $process_step_sequence = $request->post('process_step_sequence');
        $process_step_template = ProcessStepTemplate::find()->where(['process_type_id' => $process->process_type_id, 'process_step_template_sequence' => $process_step_sequence])->one();
        $process_step_template_id = $process_step_template->process_step_template_id;
        $process_step_validation_requires = ProcessStepValidationRequire::find()->where(['process_step_template_id' => $process_step_template_id])->all();
        $is_valid = true;
        foreach ($process_step_validation_requires as $process_step_validation_required) {
            $process_step_validation_required_name = $process_step_validation_required->processStepValidation->process_step_validation_name;
            $validation = new Validation();
            if (!$validation->$process_step_validation_required_name()) {
                $is_valid = false;
            }
        }
        if ($is_valid) {
            $process_steps = $process->processSteps;
            $this->next_step($process_steps, $process_step_sequence);
            $process_step_action_requires = ProcessStepActionRequire::find()->where(['process_step_template_id' => $process_step_template_id])->all();
                foreach ($process_step_action_requires as $process_step_action_require) {
                    $action = new Action();
                    $process_step_action_name = $process_step_action_require->processStepAction->process_step_action_name;
                    $action->$process_step_action_name();
                }
        }
        return $is_valid ? 'true' : 'false';
    }

    private function next_step($processSteps, $process_step_sequence)
    {
        foreach ($processSteps as $processStep) {
            if ($processStep->process_step_sequence == $process_step_sequence) {
                $newProcessStep = ProcessStep::findOne($processStep->process_step_id);
                $newProcessStep->process_step_status_id = 2;
                $newProcessStep->save();
            } else if ($processStep->process_step_sequence == $process_step_sequence + 1) {
                $newProcessStep = ProcessStep::findOne($processStep->process_step_id);
                $newProcessStep->process_step_status_id = 1;
                $newProcessStep->save();
            }
        }

    }

}