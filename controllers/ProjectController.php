<?php

namespace app\controllers;

use app\models\Project;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;

class ProjectController extends Controller
{

    public function actionIndex()
    {
        $req = Yii::$app->request;
        $session = Yii::$app->session;
        $student_id = $session->get('id');
        $project = Project::find()->where(['student_id' => $student_id])->one();
        if ($req->post()) {
            if (empty($project)) {
                $project = new Project();
                $project->student_id = $student_id;
                $project->project_name_th = $req->post('project_name_th');
                $project->project_name_eng = $req->post('project_name_eng');
                if (!$project->save())
                    return Json::encode(null);
            } else {
                $project->project_name_th = $req->post('project_name_th');
                $project->project_name_eng = $req->post('project_name_eng');
                if (!$project->save())
                    return Json::encode(null);
            }
        }
        return Json::encode($project);
    }
}
