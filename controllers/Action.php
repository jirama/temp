<?php

namespace app\controllers;

use app\models\Advisor;
use app\models\Defend;
use app\models\Semester;
use Yii;
use yii\helpers\Url;


class Action extends Validation_
{

    private $session;
    private $semester_id;

    function __construct()
    {
        $this->session = Yii::$app->session;
        $this->semester_id = Semester::find()->where(['semester_active' => 1])->one()->semester_id;
    }

    public function insert_advisor_from_petition()
    {
        $semester = Semester::find()->where(['semester_active' => 1])->one();
        $semester_id = $semester->semester_id;
        $student_id = $this->session->get('id');
        $petition_type_id = 0;
        $url = Url::home(true) . 'somebody-api/petition/' . $student_id . '/' . $petition_type_id;
        $json = $this->get_json($url);
        $main_advisor = $this->get_data($json, "$..petition_components[?(@.petition_component_name=='teacher_id_1')].petition_component_value");
        $advisor = new Advisor();
        $advisor->teacher_id = $main_advisor;
        $advisor->student_id = $student_id;
        $advisor->advisor_position_id = 0;
        $advisor->semester_id = $semester_id;
        $advisor->save();
        $addition_advisors[0] = $this->get_data($json, "$..petition_components[?(@.petition_component_name=='teacher_id_2')].petition_component_value");
        $addition_advisors[1] = $this->get_data($json, "$..petition_components[?(@.petition_component_name=='teacher_id_3')].petition_component_value");
        $addition_advisors[2] = $this->get_data($json, "$..petition_components[?(@.petition_component_name=='teacher_id_4')].petition_component_value");
        $addition_advisors[3] = $this->get_data($json, "$..petition_components[?(@.petition_component_name=='teacher_id_5')].petition_component_value");
        foreach ($addition_advisors as $addition_advisor) {
            $advisor = new Advisor();
            $advisor->teacher_id = $addition_advisor;
            $advisor->student_id = $student_id;
            $advisor->advisor_position_id = 1;
            $advisor->semester_id = $semester_id;
            $advisor->save();
        }
    }

    public function insert_proposal_defend_1_from_petition()
    {
        $semester = Semester::find()->where(['semester_active' => 1])->one();
        $semester_id = $semester->semester_id;
        $student_id = $this->session->get('id');
        $petition_type_id = 1;
        $url = Url::home(true) . 'somebody-api/petition/' . $student_id . '/' . $petition_type_id;
        $json = $this->get_json($url);
        $defend_type_id = 0;
        $defend_time_start = null;
        $defend_time_end = $defend_time_start + 1;
        $defend_date = 1 + 1 + 1;
        $defend_place_id = 1;
        $defend = new Defend();
        $defend->student_id = $student_id;
        $defend->defend_type_id = $defend_type_id;
        $defend->defend_time_start = $defend_time_start;
        $defend->defend_time_end = $defend_time_end;
        $defend->defend_date = $defend_date;
        $defend->defend_place_id = $defend_place_id;
        $defend->semester_id = $semester_id;
        $defend->save();
    }

    public function insert_committee_from_petition()
    {
    }

}
