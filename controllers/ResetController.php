<?php

namespace app\controllers;

use app\models\Advisor;
use app\models\AdvisorPosition;
use app\models\Defend;
use app\models\DefendPlace;
use app\models\DefendStatus;
use app\models\DefendType;
use app\models\DummyPerson;
use app\models\DummyPersonType;
use app\models\DummyPetition;
use app\models\DummyPetitionComponent;
use app\models\DummyPetitionStatus;
use app\models\DummyPetitionType;
use app\models\DummyPlace;
use app\models\Process;
use app\models\ProcessStep;
use app\models\ProcessStepAction;
use app\models\ProcessStepActionRequire;
use app\models\ProcessStepStatus;
use app\models\ProcessStepTemplate;
use app\models\ProcessStepValidation;
use app\models\ProcessStepValidationRequire;
use app\models\ProcessType;
use app\models\Project;
use app\models\ProjectStatus;
use app\models\Semester;
use yii\helpers\Url;
use yii\web\Controller;

class ResetController extends Controller
{
    public function actionIndex()
    {
        // DELETE ZONE
        Defend::deleteAll();
        DefendType::deleteAll();
        DefendStatus::deleteAll();
        Project::deleteAll();
        Advisor::deleteAll();
        AdvisorPosition::deleteAll();
        ProcessStep::deleteAll();
        Process::deleteAll();
        ProcessStepValidationRequire::deleteAll();
        ProcessStepValidation::deleteAll();
        ProcessStepActionRequire::deleteAll();
        ProcessStepAction::deleteAll();
        ProcessStepTemplate::deleteAll();
        ProcessType::deleteAll();
        ProcessStepStatus::deleteAll();
        Semester::deleteAll();
        DummyPetitionComponent::deleteAll();
        DummyPetition::deleteAll();
        DummyPetitionType::deleteAll();
        DummyPetitionStatus::deleteAll();
        DummyPerson::deleteAll();
        DummyPersonType::deleteAll();
        DefendPlace::deleteAll();

        //INSERT ZONE
        $this->insertDummyPersonType();
        $this->insertDummyPerson();
        $this->insertProcessType();
        $this->insertProcessStepTemplate();
        $this->insertProcessStepStatus();
        $this->insertAdvisorPosition();
        $this->insertSemester();
        $this->insertDefendType();
        $this->insertDefendStatus();
        $this->insertDummyPetitionType();
        $this->insertDummyPetitionStatus();
        $this->insertProcessStepValidation();
        $this->insertProcessStepValidationRequire();
        $this->insertProcessStepAction();
        $this->insertProcessStepActionRequire();
        $this->insertDefendPlace();

        $quote = [
            'DON\'T RUN BECAUSE YOU HAVE TO<br>RUN BECAUSE YOU CAN',
            'WITHOUT A GOAL<br>YOU CAN\'T SCORE',
        ];

        $index = rand(0, sizeof($quote) - 1);

        echo '<body style="background-color: #F15757"><h1 style="text-align: center;color: white;margin-top: 24%">
                ' . $quote[$index] . '<br>
                <a style="text-decoration: none;color:black" href="' . Url::home(true) . '">GO HOME</a>
                </h1></body>';
    }

    private function insertDummyPersonType()
    {
        $teacherType = new DummyPersonType();
        $teacherType->person_type_id = 0;
        $teacherType->person_type_name = 'นักศึกษาบัณฑิตศึกษา';
        $teacherType->save();
        $teacherType = new DummyPersonType();
        $teacherType->person_type_id = 1;
        $teacherType->person_type_name = 'อาจารย์ภายใน';
        $teacherType->save();
        $teacherType = new DummyPersonType();
        $teacherType->person_type_id = 2;
        $teacherType->person_type_name = 'อาจารย์ภายนอก';
        $teacherType->save();
        $teacherType = new DummyPersonType();
        $teacherType->person_type_id = 3;
        $teacherType->person_type_name = 'staff';
        $teacherType->save();
    }

    private function insertDummyPerson()
    {
        $student = new DummyPerson();
        $student->person_id = 'student1';
        $student->person_prefix = 'นาย';
        $student->person_first_name = 'เอ';
        $student->person_last_name = 'หนึ่ง';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();
        $student = new DummyPerson();
        $student->person_id = 'student2';
        $student->person_prefix = 'นาย';
        $student->person_first_name = 'บี';
        $student->person_last_name = 'สอง';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();
        $student = new DummyPerson();
        $student->person_id = 'student3';
        $student->person_prefix = 'นาย';
        $student->person_first_name = 'ซี';
        $student->person_last_name = 'สาม';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();
        $student = new DummyPerson();
        $student->person_id = 'student4';
        $student->person_prefix = 'นางสาว';
        $student->person_first_name = 'ดี';
        $student->person_last_name = 'สี่';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();
        $student = new DummyPerson();
        $student->person_id = 'student5';
        $student->person_prefix = 'นางสาว';
        $student->person_first_name = 'เอฟ';
        $student->person_last_name = 'หก';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();
        $student = new DummyPerson();
        $student->person_id = 'student0';
        $student->person_prefix = 'test';
        $student->person_first_name = 'test';
        $student->person_last_name = 'test';
        $student->person_type_id = 0;
        $student->person_enroll_semester = 1;
        $student->person_enroll_year = 2017;
        $student->save();

        $teacher = new DummyPerson();
        $teacher->person_id = 'teacher1';
        $teacher->person_prefix = 'ผศ.ดร.';
        $teacher->person_first_name = 'สามัญ';
        $teacher->person_last_name = 'กอ';
        $teacher->person_type_id = 1;
        $teacher->save();
        $teacher = new DummyPerson();
        $teacher->person_id = 'teacher2';
        $teacher->person_prefix = 'ผศ.ดร.';
        $teacher->person_first_name = 'เอก';
        $teacher->person_last_name = 'ขอ';
        $teacher->person_type_id = 1;
        $teacher->save();
        $teacher = new DummyPerson();
        $teacher->person_id = 'teacher3';
        $teacher->person_prefix = 'ผศ.ดร.';
        $teacher->person_first_name = 'โท';
        $teacher->person_last_name = 'คอ';
        $teacher->person_type_id = 1;
        $teacher->save();
        $teacher = new DummyPerson();
        $teacher->person_id = 'teacher4';
        $teacher->person_prefix = 'ผศ.ดร.';
        $teacher->person_first_name = 'ตรี';
        $teacher->person_last_name = 'งอ';
        $teacher->person_type_id = 1;
        $teacher->save();
        $teacher = new DummyPerson();
        $teacher->person_id = 'teacher5';
        $teacher->person_prefix = 'ผศ.ดร.';
        $teacher->person_first_name = 'จัตวา';
        $teacher->person_last_name = 'จอ';
        $teacher->person_type_id = 2;
        $teacher->save();

        $staff = new DummyPerson();
        $staff->person_id = 'staff1';
        $staff->person_prefix = 'คุณ';
        $staff->person_first_name = 'วัน';
        $staff->person_last_name = 'ทู';
        $staff->person_type_id = 3;
        $staff->save();

    }

    private function insertProcessType()
    {
        $processType = new ProcessType();
        $processType->process_type_id = 'advisor';
        $processType->process_type_name = 'แต่งตั้งอาจารย์ที่ปรึกษา';
        $processType->save();
        $processType = new ProcessType();
        $processType->process_type_id = 'proposal';
        $processType->process_type_name = 'เสนอเค้าโครง';
        $processType->save();
    }

    private function insertProcessStepTemplate()
    {
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 0;
        $processStepTemplate->process_step_template_sequence = 1;
        $processStepTemplate->process_step_template_name = 'เริ่มต้น';
        $processStepTemplate->process_type_id = 'advisor';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 1;
        $processStepTemplate->process_step_template_sequence = 2;
        $processStepTemplate->process_step_template_name = 'เพิ่มข้อมูล';
        $processStepTemplate->process_type_id = 'advisor';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 2;
        $processStepTemplate->process_step_template_sequence = 3;
        $processStepTemplate->process_step_template_name = 'ส่งคำร้อง';
        $processStepTemplate->process_type_id = 'advisor';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 3;
        $processStepTemplate->process_step_template_sequence = 4;
        $processStepTemplate->process_step_template_name = 'สำเร็จ';
        $processStepTemplate->process_type_id = 'advisor';
        $processStepTemplate->save();

        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 4;
        $processStepTemplate->process_step_template_sequence = 1;
        $processStepTemplate->process_step_template_name = 'เริ่มต้น';
        $processStepTemplate->process_type_id = 'proposal';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 5;
        $processStepTemplate->process_step_template_sequence = 2;
        $processStepTemplate->process_step_template_name = 'เพิ่มข้อมูล';
        $processStepTemplate->process_type_id = 'proposal';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 6;
        $processStepTemplate->process_step_template_sequence = 3;
        $processStepTemplate->process_step_template_name = 'ส่งคำร้อง';
        $processStepTemplate->process_type_id = 'proposal';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 7;
        $processStepTemplate->process_step_template_sequence = 4;
        $processStepTemplate->process_step_template_name = 'เข้าสอบ';
        $processStepTemplate->process_type_id = 'proposal';
        $processStepTemplate->save();
        $processStepTemplate = new ProcessStepTemplate();
        $processStepTemplate->process_step_template_id = 8;
        $processStepTemplate->process_step_template_sequence = 5;
        $processStepTemplate->process_step_template_name = 'สำเร็จ';
        $processStepTemplate->process_type_id = 'proposal';
        $processStepTemplate->save();
    }

    private function insertProcessStepStatus()
    {
        $processStepStatus = new ProcessStepStatus();
        $processStepStatus->process_step_status_id = 0;
        $processStepStatus->process_step_status_name = 'disabled';
        $processStepStatus->save();
        $processStepStatus = new ProcessStepStatus();
        $processStepStatus->process_step_status_id = 1;
        $processStepStatus->process_step_status_name = 'active';
        $processStepStatus->save();
        $processStepStatus = new ProcessStepStatus();
        $processStepStatus->process_step_status_id = 2;
        $processStepStatus->process_step_status_name = 'complete';
        $processStepStatus->save();
    }

    private function insertAdvisorPosition()
    {
        $advisor_position = new AdvisorPosition();
        $advisor_position->advisor_position_id = 0;
        $advisor_position->advisor_position_name = 'อาจารย์ที่ปรึกษาหลัก';
        $advisor_position->save();
        $advisor_position = new AdvisorPosition();
        $advisor_position->advisor_position_id = 1;
        $advisor_position->advisor_position_name = 'อาจารย์ที่ปรึกษาร่วม';
        $advisor_position->save();
    }

    private function insertSemester()
    {
        $semester = new Semester();
        $semester->semester_no = 1;
        $semester->semester_year = 2017;
        $semester->semester_active = 1;
        $semester->save();
    }

    private function insertDefendType()
    {
        $defendType = new DefendType();
        $defendType->defend_type_id = 0;
        $defendType->defend_type_name = 'สอบเค้าโครง ครั้งที่ 1';
        $defendType->save();
    }

    private function insertDummyPetitionType()
    {
        $petition = new DummyPetitionType();
        $petition->petition_type_id = 0;
        $petition->petition_type_name = 'บว.21 คำร้องขอเสนอชื่ออาจารย์ที่ปรึกษา/เปลี่ยนแปลงอาจารย์ที่ปรึกษา';
        $petition->save();
        $petition = new DummyPetitionType();
        $petition->petition_type_id = 1;
        $petition->petition_type_name = 'บว.23 แบบเสนอเค้าโครงวิทยานิพนธ์/การศึกษาอิสระ';
        $petition->save();
        $petition = new DummyPetitionType();
        $petition->petition_type_id = 2;
        $petition->petition_type_name = 'วท.บศ.05 แบบเสนอแต่งตั้งคณะกรรมการสอบเค้าโครง';
        $petition->save();
    }

    private function insertDummyPetitionStatus()
    {
        $petitionStatus = new DummyPetitionStatus();
        $petitionStatus->petition_status_id = 0;
        $petitionStatus->petition_status_name = 'ไม่สำเร็จ';
        $petitionStatus->save();
        $petitionStatus = new DummyPetitionStatus();
        $petitionStatus->petition_status_id = 1;
        $petitionStatus->petition_status_name = 'สำเร็จ';
        $petitionStatus->save();
        $petitionStatus = new DummyPetitionStatus();
        $petitionStatus->petition_status_id = 2;
        $petitionStatus->petition_status_name = 'กำลังดำเนินการ';
        $petitionStatus->save();
    }

    private function insertProcessStepValidation()
    {
        $process_step_template_validation = new ProcessStepValidation();
        $process_step_template_validation->process_step_validation_id = 0;
        $process_step_template_validation->process_step_validation_name = 'project_not_empty';
        $process_step_template_validation->save();
        $process_step_template_validation = new ProcessStepValidation();
        $process_step_template_validation->process_step_validation_id = 1;
        $process_step_template_validation->process_step_validation_name = 'advisor_petition_complete';
        $process_step_template_validation->save();
    }

    private function insertProcessStepValidationRequire()
    {
        $process_step_validation_required = new ProcessStepValidationRequire();
        $process_step_validation_required->process_step_validation_require_id = 0;
        $process_step_validation_required->process_step_template_id = 1;
        $process_step_validation_required->process_step_validation_id = 0;
        $process_step_validation_required->save();
        $process_step_validation_required = new ProcessStepValidationRequire();
        $process_step_validation_required->process_step_validation_require_id = 1;
        $process_step_validation_required->process_step_template_id = 2;
        $process_step_validation_required->process_step_validation_id = 1;
        $process_step_validation_required->save();
    }

    private function insertProcessStepAction()
    {
        $process_step_action = new ProcessStepAction();
        $process_step_action->process_step_action_id = 0;
        $process_step_action->process_step_action_name = 'insert_advisor_from_petition';
        $process_step_action->save();
    }

    private function insertProcessStepActionRequire()
    {
        $process_step_action_require = new ProcessStepActionRequire();
        $process_step_action_require->process_step_action_require_id = 0;
        $process_step_action_require->process_step_action_id = 0;
        $process_step_action_require->process_step_template_id = 2;
        $process_step_action_require->save();
    }

    private function insertDefendStatus()
    {
        $defend_status = new DefendStatus();
        $defend_status->defend_status_id = 0;
        $defend_status->defend_status_name = 'ยังไม่ได้สอบ';
        $defend_status->save();
        $defend_status = new DefendStatus();
        $defend_status->defend_status_id = 1;
        $defend_status->defend_status_name = 'กำลังสอบ';
        $defend_status->save();
        $defend_status = new DefendStatus();
        $defend_status->defend_status_id = 2;
        $defend_status->defend_status_name = 'สอบเสร็จแล้ว';
        $defend_status->save();
    }

    private function insertDefendPlace()
    {
        $defend_place = new DefendPlace();
        $defend_place->defend_place_id = 0;
        $defend_place->defend_place_name = '8080';
        $defend_place->save();
    }
}