<?php
namespace app\controllers;

use app\models\Advisor;
use app\models\Project;
use Codeception\Util\Xml;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    public function actionAdvisor($student_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $advisor = Advisor::find()->where(['student_id' => $student_id])->all();
        return ArrayHelper::toArray($advisor, [
            'app\models\Advisor' => [
                'advisor_id',
                'student_id',
                'advisor_position' => function ($model) {
                    return $model->advisorPosition;
                },
                'teacher' => function ($model) {
                    $url = Url::home(true) . 'somebody-api/person/' . $model->teacher_id;
                    $json = file_get_contents($url);
                    return Json::decode($json);
                },
            ],
        ]);
    }
}
