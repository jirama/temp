<?php

namespace app\controllers;

use app\models\Defend;
use app\models\DefendPlace;
use app\models\Project;
use app\models\Semester;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;

class DefendController extends Controller
{
    public function actionIndex($defend_type_id)
    {
        $req = Yii::$app->request;
        $session = Yii::$app->session;
        $student_id = $session->get('id');
        $defend = Defend::find()->where(['student_id' => $student_id, 'defend_type_id' => $defend_type_id])->one();
        if ($req->post()) {
            $semester = Semester::find()->where(['semester_active' => 1])->one();
            $project = Project::find()->where(['student_id' => $student_id])->one();
            if (empty($defend)) {
                $defend = new Defend();
                $defend->student_id = $student_id;
                $defend->defend_type_id = $defend_type_id;
                $defend->defend_date = $req->post('defend_date');
                $defend->defend_time_start = $req->post('defend_time_start');
                $defend->defend_time_end = $req->post('defend_time_end');
                $defend->defend_place_id = $req->post('defend_place_id');
                $defend->semester_id = $semester->semester_id;
                $defend->project_id = $project->project_id;
                $defend->defend_status_id = 0;
                if (!$defend->save())
                    return Json::encode(null);
            } else {
                $defend->defend_date = $req->post('defend_date');
                $defend->defend_time_start = $req->post('defend_time_start');
                $defend->defend_time_end = $req->post('defend_time_end');
                $defend->defend_place_id = $req->post('defend_place_id');
                if (!$project->save())
                    return Json::encode(null);
            }
        }
        return Json::encode($defend);
    }

    public function actionPlace()
    {
        $defend_place = DefendPlace::find()->all();
        return Json::encode($defend_place);
    }
}
//        date_default_timezone_set('Asia/Bangkok');
//        $dateNow = date('d/m/Y');
//        $timeNow = date('H:i');
//        $defend = Defend::find()
//            ->where(['defend_date' => $dateNow])
//            ->andWhere(['<', 'defend_time_start', $timeNow])
//            ->andWhere(['>', 'defend_time_end', $timeNow])
//            ->one();
//        $data['defend'] = $defend;
//        return $this->render('index', $data);


