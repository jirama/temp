<?php

namespace app\controllers;

use app\models\DummyPerson;
use app\models\DummyPetition;
use app\models\DummyPetitionComponent;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class SomebodyApiController extends Controller
{

    public function actionPerson($person_id)
    {
        $person = DummyPerson::findOne($person_id);
        return Json::encode(ArrayHelper::toArray($person, [
            'app\models\DummyPerson'
        ]));
    }

    public function actionPetition($studentId, $petitionTypeId)
    {
        $petition = DummyPetition::find()->where(['student_id' => $studentId, 'petition_type_id' => $petitionTypeId, 'petition_active' => 1])->one();
        return Json::encode(ArrayHelper::toArray($petition, [
            'app\models\DummyPetition' => [
                'petition_id',
                'petition_active',
                'student_id',
                'petition_type' => function ($model) {
                    return $model->petitionType;
                },
                'petition_status' => function ($model) {
                    return $model->petitionStatus;
                },
                'petition_components' => function ($model) {
                    return $model->dummyPetitionComponents;
                },
            ],
        ]));
    }
}
