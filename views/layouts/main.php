<?php
use app\models\DummyPerson;
use app\models\ProcessType;
use yii\helpers\Url;

$session = Yii::$app->session;

?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>E-GS</title>
    <link rel="icon" href="<?= Yii::$app->homeUrl ?>images/doggo.jpg" type="image/gif" sizes="16x16">

    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>

    <!-- WEB FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext"
          rel="stylesheet" type="text/css"/>

    <!-- CORE CSS -->
    <link href="<?= Yii::$app->homeUrl ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- THEME CSS -->
    <link href="<?= Yii::$app->homeUrl ?>css/essentials.css" rel="stylesheet" type="text/css"/>
    <link href="<?= Yii::$app->homeUrl ?>css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?= Yii::$app->homeUrl ?>css/color_scheme/green.css" rel="stylesheet" type="text/css"
          id="color_scheme"/>

    <!-- JQUERY -->
    <script type="text/javascript" src="<?= Yii::$app->homeUrl ?>plugins/jquery/jquery-2.1.4.min.js"></script>
</head>
<body>
<div class="text-"></div>
<!-- WRAPPER -->
<div id="wrapper">
    <aside id="aside">
        <nav id="sideNav">
            <ul class="nav nav-list">
                <li>
                    <a class="dashboard" href="<?= Yii::$app->homeUrl ?>reset" style="color: red">
                        <i class="main-icon fa fa-exclamation-triangle"></i> <span>reset database</span>
                    </a>
                </li>
                <li>
                    <a class="dashboard" href="<?= Yii::$app->homeUrl ?>">
                        <i class="main-icon fa fa-home"></i> <span>หน้าแรก</span>
                    </a>
                </li>
                <li class="el_primary" id="el_1">
                    <a href="#">
                        <i class="fa fa-menu-arrow pull-right"></i>
                        <i class="main-icon fa-spin fa fa-refresh"></i> <span>การจัดการ</span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?= Yii::$app->homeUrl ?>process">หน้าแรกจ้า</a>
                        </li>
                        <?php $processTypes = ProcessType::find()->all(); ?>
                        <?php foreach ($processTypes as $processType): ?>
                            <li>
                                <a href="<?= Yii::$app->homeUrl ?>process/<?= $processType->process_type_id ?>"><?= $processType->process_type_name ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li>
                    <a href="<?= Yii::$app->homeUrl ?>test">
                        <i class="main-icon fa fa-legal"></i><span>process shortcut</span>
                    </a>
                </li>
            </ul>
        </nav>

        <span id="asidebg"><!-- aside fixed background --></span>
    </aside>
    <!-- /ASIDE -->

    <!-- HEADER -->
    <header id="header">
        <!-- Mobile Button -->
        <button id="mobileMenuBtn"></button>
        <!-- Logo -->
        <span class="logo pull-left">
                <img src="<?= Yii::$app->homeUrl ?>images/logo_light.png" alt="admin panel" height="35"/>
            </span>
        <nav>
            <ul class="nav pull-right">
                <li class="dropdown pull-left">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding: 0 20px !important;">
                        <i class="glyphicon glyphicon-user" style="margin-top: 15px"></i>
                    </a>
                    <ul class="dropdown-menu hold-on-click">
                        <?php if ($session->has('id')): ?>
                            <li>
                                <a href="#me">
                                    <?= $session->get('name') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl ?>dummy-person/logout">Logout</a>
                            </li>
                        <?php else: ?>
                            <?php $persons = DummyPerson::find()->all(); ?>
                            <?php if (!empty($persons)): ?>
                                <?php $personTypeId = $persons[0]['person_type_id'] ?>
                                <?php foreach ($persons as $person): ?>
                                    <?php if ($personTypeId != $person['person_type_id']): ?>
                                        <li class="divider"></li>
                                    <?php endif; ?>
                                    <?php $personTypeId = $person['person_type_id'] ?>
                                    <li>
                                        <a href="<?= Yii::$app->homeUrl ?>dummy-person/login/<?= $person['person_id'] ?>"><?= $person['person_first_name'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <!-- /HEADER -->

    <!-- MIDDLE -->
    <section id="middle" class="section">
        <header id="page-header" class="loading">
            <h1><?= $this->params['title'] ?></h1>
        </header>
        <div id="content" class="padding-20 loading">
            <?= $content ?>
        </div>
    </section>
</div>
<script type="text/javascript">var plugin_path = '<?= Yii::$app->homeUrl ?>plugins/';</script>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>js/app.js"></script>
</body>
</html>
