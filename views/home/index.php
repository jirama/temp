<?php
Yii::$app->view->params['title'] = 'หน้าแรก';
setlocale(LC_TIME, "th-TH");
?>

<div class="col-sm-3">
    <div class="slider-wrapper success-slider">
        <div id="slider"></div>
    </div>
</div>
<!--<div id="time-range"></div>-->
<div id="test"></div>

<script type="text/javascript">
    $(window).ready(function () {
        loadScript(plugin_path + 'jquery/jquery-ui.min.js', function () {
            loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function () {
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function () {
                    var min = '08:30';
                    var max = '16:30';
                    $('#test').html(min + ' - ' + min);
                    var diff = Math.abs(new Date('1995/04/16 ' + min) - new Date('1995/04/16 ' + max));
                    var slide_max = Math.floor((diff / 1000) / 60) / 10;
                    var slider = $("#slider").slider({
                        range: true,
                        animate: true,
                        min: 0,
                        max: slide_max,
                        slide: function (event, ui) {
                            var date = new Date('1995/04/16 ' + min);
                            start = new Date(date.getTime() + ui.values[0] * 10 * 60000);
                            end = new Date(date.getTime() + ui.values[1] * 10 * 60000);
                            var time_start = (start.getHours() < 10 ? '0' : '') + start.getHours() + ':' + (start.getMinutes() < 10 ? '0' : '') + start.getMinutes();
                            var time_end = (end.getHours() < 10 ? '0' : '') + end.getHours() + ':' + (end.getMinutes() < 10 ? '0' : '') + end.getMinutes();
                            var time = time_start + ' - ' + time_end;
                            $('#test').html(time);
                        },
                        change: function () {
                            console.log('triggered');
                        }
                    });
                });
            });
        });
    });
</script>
