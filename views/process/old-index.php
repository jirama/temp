<?php
Yii::$app->view->params['title'] = 'หน้าแรกจ้า';
?>
<script src="<?= Yii::$app->homeUrl ?>plugins/degre-d3/d3.v3.js"></script>
<script src="<?= Yii::$app->homeUrl ?>plugins/degre-d3/degre-d3.js"></script>

<style>
    g.type-TK > rect {
        fill: #f5f5f5;
        stroke: green;
    }

    .node rect {
        stroke: #999;
        fill: #fff;
        stroke-width: 2px;
    }

    .edgePath path {
        fill: #f66;
        stroke: #f66;
        stroke-width: 1px;
    }

    .dafaq {
        color: red;
        fill: black;
    }

</style>
<svg id="svg-canvas" style="height: auto"></svg>

<script>
    $(document).ready(function () {
        $('#lolz').click(function () {
            alert('5555');
        });
    });

    var mySVG = document.getElementById("svg-canvas");
    var aside = $('#aside')
    mySVG.setAttribute("width", window.innerWidth);
    mySVG.setAttribute("height", window.innerHeight);

    // Create the input graph
    var g = new dagreD3.graphlib.Graph()
        .setGraph({})
        .setDefaultEdgeLabel(function () {
            return {};
        });

    g.setNode(0, {
        labelType: "html",
        label: "dafaq",
        style: "fill: black",
        class: "dafaq",
        id: "lolz"
    });
    g.setNode(1, {label: "S", class: "dafaq"});
    g.setNode(2, {label: "NP", class: "type-NP"});
    g.setNode(3, {label: "DT", class: "type-DT"});
    g.setNode(4, {label: "This", class: "type-TK"});
    g.setNode(5, {label: "VP", class: "type-VP"});
    g.setNode(6, {label: "VBZ", class: "type-VBZ"});
    g.setNode(7, {label: "is", class: "type-TK"});
    g.setNode(8, {label: "NP", class: "type-NP"});
    g.setNode(9, {label: "DT", class: "type-DT"});
    g.setNode(10, {label: "an", class: "type-TK"});
    g.setNode(11, {label: "NN", class: "type-NN"});
    g.setNode(12, {label: "example", class: "type-TK"});
    g.setNode(13, {label: "asdasdasdasdsadasdasdasdasd", class: "type-."});
    g.setNode(14, {label: "sentence", class: "type-TK"});
    g.setNode(15, {label: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", class: "type-TK"});
    g.setNode(16, {label: "x", class: "type-TK"});
    g.setNode(17, {label: "xx", class: "type-TK"});
    g.setNode(18, {label: "xxx", class: "type-TK"});
    g.setNode('love1', {label: "xxx", class: "type-TK"});

    // Set up edges, no special attributes.
    g.setEdge(3, 4);
    g.setEdge(2, 3);
    g.setEdge(1, 2);
    g.setEdge(6, 7);
    g.setEdge(5, 6);
    g.setEdge(9, 10);
    g.setEdge(8, 9);
    g.setEdge(11, 12);
    g.setEdge(8, 11);
    g.setEdge(5, 8);
    g.setEdge(1, 5);
    g.setEdge(13, 14);
    g.setEdge(1, 13);
    g.setEdge(0, 1);
    g.setEdge(12, 15);
    g.setEdge(10, 15);
    g.setEdge(15, 16);
    g.setEdge(15, 17);
    g.setEdge(15, 18);
    g.setEdge(15, 'love1');

    g.nodes().forEach(function (v) {
        var node = g.node(v);
        // Round the corners of the nodes
        node.rx = node.ry = 5;
    });

    // Create the renderer
    var render = new dagreD3.render();

    // Set up an SVG group so that we can translate the final graph.
    var svg = d3.select("svg"),
        svgGroup = svg.append("g");

    // Run the renderer. This is what draws the final graph.
    render(d3.select("svg g"), g);

    // Center the graph
    var xCenterOffset = (svg.attr("width") - g.graph().width) / 2;
    console.log(xCenterOffset);
    svgGroup.attr("transform", "translate(" + xCenterOffset / 2 + ", 10)");
    svg.attr("height", g.graph().height + 40);
</script>
