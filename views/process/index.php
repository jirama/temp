<?php
/* @var app\models\Project $project */
Yii::$app->view->params['title'] = 'หน้าแรกจ้า';
?>
<div style="display: none">
    <!--<script src="--><? //= Yii::$app->homeUrl ?><!--plugins/ckeditor/ckeditor.js"></script>-->
    <!---->
    <!--<form action="--><? //= Yii::$app->homeUrl ?><!--test/temp" method="post">-->
    <!--    <textarea name="test" id="editor1">-->
    <!--	</textarea>-->
    <!--    <input type="submit" class="btn btn-success" value="OKAY">-->
    <!--</form>-->
    <!---->
    <!--<script>-->
    <!--    CKEDITOR.replace('editor1', {-->
    <!--        enterMode: CKEDITOR.ENTER_BR,-->
    <!--        allowedContent: true,-->
    <!--        bodyClass: 'document-editor'-->
    <!--    });-->
    <!--</script>-->
</div>

<?php if ($project === null) { ?>
    <form method="post" action="<?= Yii::$app->homeUrl ?>project/insert">
        <div class="form-group">
            <label for="project-name-th">ชื่อหัวข้อที่ศึกษาภาษาไทย</label>
            <input type="text" id="project-name-th" name="project-name-th" class="form-control">
        </div>
        <div class="form-group">
            <label for="project-name-eng">ชื่อหัวข้อที่ศึกษาภาษาอังกฤษ</label>
            <input type="text" id="project-name-eng" name="project-name-eng" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success btn-3d" value="ตกลง">
        </div>
    </form>
<?php } else { ?>
    <div class="table-responsive" id="project-detail">
        <table class="table table-hover">
            <tbody>
            <tr>
                <td><strong>ชื่อหัวข้อภาษาไทย</strong></td>
                <td><?= $project->project_name_th ?></td>
            </tr>
            <tr>
                <td><strong>ชื่อหัวข้อภาษาอังกฤษ</strong></td>
                <td><?= $project->project_name_eng ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <button id="go-edit" class="btn btn-3d btn-sm btn-white">แก้ไข</button>
    <form id="edit" method="post" action="<?= Yii::$app->homeUrl ?>project/edit" style="display: none">
        <input type="hidden" name="project-id" value="<?= $project->project_id ?>">
        <div class="form-group">
            <label for="project-name-th">ชื่อหัวข้อที่ศึกษาภาษาไทย</label>
            <input type="text" id="project-name-th" name="project-name-th" value="<?= $project->project_name_th ?>"
                   class="form-control">
        </div>
        <div class="form-group">
            <label for="project-name-eng">ชื่อหัวข้อที่ศึกษาภาษาอังกฤษ</label>
            <input type="text" id="project-name-eng" name="project-name-eng" value="<?= $project->project_name_eng ?>"
                   class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success btn-3d" value="ตกลง">
        </div>
    </form>
<?php } ?>
<a href="<?=Yii::$app->homeUrl?>test/auto-add-project" class="btn btn-3d btn-white">ใส่หัวข้ออัตโนมัติ</a>

<script>
    $('#go-edit').click(function () {
        $('#edit').fadeIn();
    });
</script>