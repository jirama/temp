<?php
/* @var $process app\models\Process */

Yii::$app->view->params['title'] = $process->processType->process_type_name;
$processSteps = $process->processSteps;
Yii::$app->view->params['processSteps'] = $processSteps;
?>
<style>
    .form-control, .form-control-sm{
        border-top: none;
        border-left: none;
        border-right: none;
        width: 90%;
        margin: 0 auto;
        text-align: center;
        text-align-last: center;
    }

    .additional-data td {
        text-align: center;
    }

    .right {
        text-align: right;
    }

    #form td {
        width: 50%;
    }

    input, select {
    }

</style>
<div class="row process-wizard process-wizard-primary">
    <?php foreach ($processSteps as $processStep): ?>
        <div id="step<?= $processStep->process_step_sequence ?>"
             class="col-xs-3 process-wizard-step <?= $processStep->processStepStatus->process_step_status_name ?>">
            <div class="text-center process-wizard-stepnum">
                ขั้นตอนที่ <?= $processStep->process_step_sequence ?>
            </div>
            <div class="progress">
                <div class="progress-bar"></div>
            </div>
            <div class="process-wizard-dot"></div>
            <div class="process-wizard-info text-center"><?= $processStep->process_step_name ?></div>
        </div>
    <?php endforeach; ?>
</div>

<div class="process-body">
    <?php
    foreach ($processSteps as $processStep) {
        if ($processStep->process_step_status_id == 1) { ?>
            <?php Yii::$app->runAction('process-step', ['processId' => $processStep->process_id, 'processStepSequence' => $processStep->process_step_sequence]); ?>
            <?php
        }
    } ?>
</div>

<script>

    function btn_is_edit() {
        var btn = $('#form_btn');
        btn.attr('type', 'button');
        btn.html('แก้ไขข้อมูล');
        if (btn.hasClass('btn-green')) {
            btn.toggleClass('btn-green btn-blue');
        } else {
            btn.addClass('btn-blue');
        }
    }

    function btn_is_insert() {
        var btn = $('#form_btn');
        btn.attr('type', 'submit');
        btn.html('ตกลง');
        if (btn.hasClass('btn-blue')) {
            btn.toggleClass('btn-blue btn-green');
        } else {
            btn.addClass('btn-green');
        }
    }

    function set_form_data() {
        $.getJSON(url_for_form, function (data) {
            var json = data;
            var input = $('#form').find('input');
            $.each(input, function () {
                var input = $(this);
                var key = input.attr('id');
                var data = input.siblings('span');
                if (json === null) {
                    data.hide();
                    btn_is_insert();
                } else {
                    input.val(json[key]);
                    input.hide();
                    data.html(json[key]);
                    btn_is_edit();
                }
            });
            process_body.fadeIn();
        });
    }

    function form_submit() {
        var input = $('#form').find('input');
        var data = {};
        $.each(input, function () {
            var input = $(this);
            var key = input.attr('id');
            data[key] = input.val();
        });
        $.post(url_for_form, data, function (data) {
            var json = data;
            if (json === null) {
                alert('nope');
            } else {
                $.each(input, function () {
                    var input = $(this);
                    var key = input.attr('id');
                    var data = input.siblings('span');
                    input.val(json[key]);
                    data.html(json[key]);
                    input.fadeOut(function () {
                        data.fadeIn();
                    });
                    var form_btn = $('#form_btn');
                    form_btn.fadeOut(function () {
                        btn_is_edit();
                        form_btn.fadeIn();
                    });
                });
            }
        }, 'json');
    }

    function form_insert(elm) {
        console.log(elm.attr('type'));
        if (elm.attr('type') === 'button') {
            console.log('fuck you');
            var input = $('#form').find('input');
            $.each(input, function () {
                var input = $(this);
                var data = input.siblings('span');
                data.fadeOut(function () {
                    input.fadeIn();
                });
            });
            elm.fadeOut(function () {
                btn_is_insert();
                elm.fadeIn();
            });
        }
    }

    function validate() {
        var url = '<?= Yii::$app->homeUrl . 'process-step/validation' ?>';
        var data = {
            process_id: process_id,
            process_step_sequence: process_step_sequence
        };
        var next_step_sequence = parseInt(process_step_sequence) + 1;
        $.post(url, data, function (data) {
            if (data === 'true') {
                $('#step' + process_step_sequence).attr('class', 'col-xs-3 process-wizard-step complete');
                setTimeout(function () {
                    $('#step' + next_step_sequence).attr('class', 'col-xs-3 process-wizard-step active');
                }, 500);
                var url = '<?= Yii::$app->homeUrl . 'process-step/' . $process->process_id . '/'?>' + next_step_sequence;
                process_body.fadeOut();
                setTimeout(function () {
                    $.get(url, function (data) {
                        process_body.html(data);
                    });
                }, 500);
            } else {
                alert(data);
            }
        });
    }

</script>

