<?php
/* @var $processStep app\models\ProcessStep */
?>
<div class="process-header">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content">
    <div class="tab">
        <small>
            text text text text text text text text text text text text text text text text text text text text text
            text text text text text text text text text text text text text text text text text text text text text
            text text text text text text text text text text text text text text text text text text text text text
        </small>
    </div>
    <div class="tab">
        <small>
            text text text text text text text text text text text text text text text text text text text text text
            text text text text text text text text text text text text text text text text text text text text text
            text text text text text text text text text text text text text text text text text text text text text
        </small>
    </div>
    <div>
        <button class="btn btn-white btn-sm btn-3d"><i class="fa fa-file-text-o"></i>รายละเอียด</button>
        <button class="btn btn-white btn-sm btn-3d"><i class="fa fa-tasks"></i>ตรวจสอบภาระอาจารย์</button>
    </div>
</div>
<div class="right">
    <button class="btn btn-lg btn-3d btn-green" id="next-step">
        ขั้นตอนต่อไป <i class="fa fa-arrow-right"></i>
    </button>
</div>
<script>
    var process_body = $('.process-body');
    var process_id = '<?= $processStep->process_id ?>';
    var process_step_sequence = '<?= $processStep->process_step_sequence ?>';

    $(document).ready(function () {
        process_body.hide();
        process_body.fadeIn()
    });

    $('#next-step').click(function () {
        validate();
    });

</script>