<?php $session = Yii::$app->session ?>
<div class="process-header text-center">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content text-center">

    <div>รายชื่ออาจารย์ที่ปรึกษาของคุณ</div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center">ชื่ออาจารย์</th>
                <th class="text-center">ตำแหน่ง</th>
            </tr>
            </thead>
            <tbody id="teacher">
            </tbody>
        </table>
    </div>
</div>
<script>
    var process_body = $('.process-body');
    var url = '<?= Yii::$app->homeUrl . 'api/advisor/' . $session->get('id') ?>';

    $(document).ready(function () {
        process_body.hide();
        set_page_data();
    });

    function set_page_data() {
        $.getJSON(url, function (data) {
            for (var i = 0; i < data.length; i++) {
                var advisor = data[i];
                var teacher = advisor['teacher'];
                var teacher_name = teacher['person_prefix'] + ' ' + teacher['person_first_name'] + ' ' + teacher['person_last_name'];
                var teacher_position = advisor['advisor_position']['advisor_position_name'];
                var tr = '<tr><td>' + teacher_name + '</td><td>' + teacher_position + '</td></tr>';
                $('#teacher').append(tr);
            }
            process_body.fadeIn();
        });
    }
</script>
