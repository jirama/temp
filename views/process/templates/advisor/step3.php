<?php
/* @var $processStep app\models\ProcessStep */
$session = Yii::$app->session;
?>
<div class="process-header text-center">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content">
    <div class="padding-10">ข้อมูลคำร้องแต่งตั้งอาจารย์</div>
    <div class="table-responsive text-center">
        <table id="table" class="table table-hover">
            <tbody>
            <tr>
                <td><strong>รหัสใบคำร้อง</strong></td>
                <td><span id="pet-id">n/a</span></td>
            </tr>
            <tr>
                <td><strong>ประเภทใบคำร้อง</strong></td>
                <td><span id="pet-type">n/a</span></td>
            </tr>
            <tr>
                <td><strong>สถานะใบคำร้อง</strong></td>
                <td><span id="pet-status">n/a</span></td>
            </tr>
            <tr>
                <td><strong>รายละเอียดคำร้อง</strong></td>
                <td><a target="_blank"
                       href="<?= Yii::$app->homeUrl ?>somebody-api/petition/<?= $session->get('id') ?>/0"
                       class="btn btn-3d btn-xs btn-white">ไปหน้าคำร้อง</a></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="text-center">
        <a href="<?= Yii::$app->homeUrl ?>dummy-petition/insert-advisor" class="btn btn-3d btn-dirtygreen">ส่งคำร้องแต่งตั้งอาจารย์</a>
    </div>
    <div class="right">
        <button class="btn btn-lg btn-3d btn-green" id="next-step">
            ขั้นตอนต่อไป <i class="fa fa-arrow-right"></i>
        </button>
    </div>
    <a href="<?= Yii::$app->homeUrl ?>dummy-petition/complete/0"
       class="btn btn-3d btn-red">ทำให้คำร้องแต่งตั้งอาจารย์สำเร็จ</a>
</div>
<script>
    var process_body = $('.process-body');
    var process_id = '<?= $processStep->process_id ?>';
    var process_step_sequence = '<?= $processStep->process_step_sequence ?>';

    $(document).ready(function () {
        process_body.hide();
        var pet_type = '0';
        var url = '<?= Yii::$app->homeUrl . 'somebody-api/petition/' . $session->get('id') ?>/' + pet_type;
        set_page_data(url);
    });

    $('#next-step').click(function () {
        validate();
    });

    function set_page_data(url) {
        $.getJSON(url, function (data) {
                var pet_id = $('#pet-id');
                var pet_type = $('#pet-type');
                var pet_status = $('#pet-status');
                if (data[0] === null) {
                    pet_id.addClass('label label-primary');
                    pet_type.addClass('label label-primary');
                    pet_status.addClass('label label-primary');
                } else {
                    pet_id.html(data['petition_id']);
                    pet_type.html(data['petition_type']['petition_type_name']);
                    pet_status.html(data['petition_status']['petition_status_name']);
                    switch (data['petition_status']['petition_status_id']) {
                        case 0:
                            pet_status.addClass('label label-danger');
                            break;
                        case 1:
                            pet_status.addClass('label label-success');
                            break;
                        case 2:
                            pet_status.addClass('label label-info');
                            break;
                    }
                }
                process_body.fadeIn();
            }
        );
    }
</script>
