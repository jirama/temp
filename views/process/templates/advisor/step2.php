<?php
/* @var $processStep app\models\ProcessStep */
$session = Yii::$app->session;
?>
<div class="process-header text-center">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content">
    <form id="form">
        <div class="table-responsive text-center">
            <table id="table" class="table table-hover">
                <tbody>
                <tr>
                    <td><label for="project_name_th"><strong>ชื่อเรื่องที่ศึกษาภาษาไทย</strong></label></td>
                    <td>
                        <input id="project_name_th" type="text" class="form-control-sm text-center"/>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td><label for="project_name_eng"><strong>ชื่อเรื่องที่ศึกษาภาษาอังกฤษ</strong></label></td>
                    <td>
                        <input id="project_name_eng" type="text" class="form-control-sm text-center"/>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button id="form_btn" class="btn btn-xs btn-3d"></button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="right">
        <button class="btn btn-lg btn-3d btn-green" id="next-step">
            ขั้นตอนต่อไป <i class="fa fa-arrow-right"></i>
        </button>
    </div>
</div>
<script>
    var process_body = $('.process-body');
    var process_id = '<?= $processStep->process_id ?>';
    var process_step_sequence = '<?= $processStep->process_step_sequence ?>';
    var url_for_form = '<?= Yii::$app->homeUrl ?>project';

    $(document).ready(function () {
        process_body.hide();
        set_form_data();
    });

    $('#form_btn').click(function () {
        var elm = $(this);
        form_insert(elm);
    });
    $('#next-step').click(function () {
        validate();
    });

    $('#form').on('submit', function () {
        form_submit();
        return false;
    });


</script>
