<?php
/* @var $processStep app\models\ProcessStep */
$session = Yii::$app->session;
?>
<?php setlocale(LC_ALL, 'th_TH.UTF-8'); ?>
<style>
    .scrollable-menu {
        margin: 0;
        padding: 0;
        height: auto;
        max-height: 500%;
        overflow-y: scroll;
    }

    .scrollable-menu li {
        word-wrap: break-word;
        padding: 5px 20px;
    }

    .scrollable-menu li:hover {
        cursor: pointer;
        background-color: #f5f5f5;
    }

    .input-dropdown {
        width: 100%;
    }

</style>
<div class="process-header text-center">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content">
    <form id="form">
        <div class="table-responsive text-center">
            <table id="table" class="table table-hover">
                <tbody>
                <tr>
                    <td><label for="defend_date"><strong>วันสอบ</strong></label></td>
                    <td>
                        <input type="text" id="defend_date" class="form-control-sm text-center datepicker" readonly>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td><label for="defend_place_id"><strong>สถานที่สอบ</strong></label></td>
                    <td>
                        <div class="select-wrap">
                        <select id="defend_place_id" class="form-control input-sm">
                            <option value="x">x</option>
                            <option value="y">yyyyyyyyyyy</option>
                            <option value="z">zzzzzz</option>
                        </select>
                        </div>
                        <!--                        <div class="btn-group input-dropdown">-->
                        <!--                            <input type="text" name="defend_place_id" class="form-control-sm text-center"-->
                        <!--                                   data-toggle="dropdown" readonly>-->
                        <!--                            <ul class="dropdown-menu scrollable-menu" role="menu" style="width: inherit">-->
                        <!--                                <li value="x">else here</li>-->
                        <!--                            </ul>-->
                        <!--                        </div>-->

                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="slider-wrapper success-slider" style="width: 60%;margin: 4% auto;">
                            <div id="slider"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><label for="defend_time_start"><strong>เวลาเริ่มสอบ</strong></label></td>
                    <td>
                        <input id="defend_time_start" type="text" class="form-control-sm text-center" readonly/>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td><label for="defend_time_end"><strong>เวลาสิ้นสุดการสอบ</strong></label></td>
                    <td>
                        <input id="defend_time_end" type="text" class="form-control-sm text-center" readonly/>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button id="form_btn" class="btn btn-xs btn-3d"></button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="right">
        <button class="btn btn-lg btn-3d btn-green" id="next-step">
            ขั้นตอนต่อไป <i class="fa fa-arrow-right"></i>
        </button>
    </div>
</div>
<script>
    var process_body = $('.process-body');
    var process_id = '<?= $processStep->process_id ?>';
    var process_step_sequence = '<?= $processStep->process_step_sequence ?>';
    var url_for_form = '<?= Yii::$app->homeUrl ?>defend/0';
    var form_select_value = null;

    $('.input-dropdown').find('li').click(function () {
        $('.input-dropdown').find('input').val($(this).html());
    });

    $(document).ready(function () {
        process_body.hide();
        loadScript(plugin_path + 'bootstrap.datepicker/js/bootstrap-datepicker.js', function () {
//            loadScript(plugin_path + 'bootstrap.datepicker/locales/bootstrap-datepicker.th.min.js', function () {
            var datepicker = $('.datepicker').datepicker({
                format: 'dd MM yyyy',
                language: 'th'
            });
            datepicker.datepicker('setDate', new Date());
//            });
        });

        loadScript(plugin_path + 'jquery/jquery-ui.min.js', function () {
            loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function () {
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function () {
                    var min = '08:30';
                    var max = '17:00';
                    $('#defend_time_start').val(min);
                    $('#defend_time_end').val(min);
                    var date_min = new Date('1995/04/16 ' + min);
                    var date_max = new Date('1995/04/16 ' + max);
                    var diff = Math.abs(date_min - date_max);
                    var slide_max = Math.floor((diff / 1000) / 60) / 10;
                    var slider = $("#slider").slider({
                        range: true,
                        animate: true,
                        min: 0,
                        max: slide_max,
                        values: [0, Math.floor(slide_max / 2)],
                        create: function () {
                            var values = $(this).slider('values');
                            set_input_time(values);
                        },
                        slide: function (event, ui) {
                            var values = ui.values;
                            set_input_time(values);
                        }
                    });
                    slider.slider("pips", {rest: false});
                    $('.ui-slider-pip-0').find('span.ui-slider-label').html(min);
                    $('.ui-slider-pip-' + slide_max).find('span.ui-slider-label').html(max);

                    function set_input_time(values) {
                        var date = new Date('1995/04/16 ' + min);
                        start = new Date(date.getTime() + values[0] * 10 * 60 * 1000);
                        end = new Date(date.getTime() + values[1] * 10 * 60 * 1000);
                        var time_start = (start.getHours() < 10 ? '0' : '') + start.getHours() + ':' + (start.getMinutes() < 10 ? '0' : '') + start.getMinutes();
                        var time_end = (end.getHours() < 10 ? '0' : '') + end.getHours() + ':' + (end.getMinutes() < 10 ? '0' : '') + end.getMinutes();
                        $('#defend_time_start').val(time_start);
                        $('#defend_time_end').val(time_end);
                    }
                });
            });
        });
        set_form_data();
        set_page_data();
    });

    function set_page_data() {
        url = '<?= Yii::$app->homeUrl ?>defend/place';
        $.getJSON(url, function (data) {
            $.each(data, function (key, val) {
                var select = $('#defend_date').siblings('ul');
                var li = ('<li/>', {});
                select.append()
                console.log(val);
            });
        });
    }

    $('#form_btn').click(function () {
        var elm = $(this);
        form_insert(elm);
    });
    $('#next-step').click(function () {
        validate();
    });

    $('#form').on('submit', function () {
        form_submit();
        return false;
    });
</script>
