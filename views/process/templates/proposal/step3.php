<?php
/* @var $processStep app\models\ProcessStep */
$session = Yii::$app->session;
?>
<div class="process-header text-center">
    <h4><?= $processStep->process_step_name ?></h4>
</div>
<div class="process-content">
    <div class="padding-10">ข้อมูลคำร้องขอสอบ</div>
    <div class="table-responsive text-center margin-bottom-0">
        <table id="table" class="table table-hover">
            <tbody>
            <tr>
                <td><strong>รหัสใบคำร้อง</strong></td>
                <td><span id="pet-id-1">n/a</span></td>
            </tr>
            <tr>
                <td><strong>ประเภทใบคำร้อง</strong></td>
                <td><span id="pet-type-1">n/a</span></td>
            </tr>
            <tr>
                <td><strong>สถานะใบคำร้อง</strong></td>
                <td><span id="pet-status-1">n/a</span></td>
            </tr>
            <tr>
                <td><strong>รายละเอียดคำร้อง</strong></td>
                <td>
                    <a target="_blank"
                       href="<?= Yii::$app->homeUrl ?>somebody-api/petition/<?= $session->get('id') ?>/1"
                       class="btn btn-3d btn-xs btn-white">ไปหน้าคำร้อง</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="text-center">
        <a href="<?= Yii::$app->homeUrl ?>dummy-petition/insert-proposal"
           class="btn btn-3d btn-dirtygreen">ส่งคำร้องขอสอบ</a>
    </div>
    <div class="padding-10">ข้อมูลคำร้องขอแต่งตั้งกรรมการสอบ</div>
    <div class="table-responsive text-center">
        <table id="table" class="table table-hover">
            <tbody>
            <tr>
                <td><strong>รหัสใบคำร้อง</strong></td>
                <td><span id="pet-id-2">n/a</span></td>
            </tr>
            <tr>
                <td><strong>ประเภทใบคำร้อง</strong></td>
                <td><span id="pet-type-2">n/a</span></td>
            </tr>
            <tr>
                <td><strong>สถานะใบคำร้อง</strong></td>
                <td><span id="pet-status-2">n/a</span></td>
            </tr>
            <tr>
                <td><strong>รายละเอียดคำร้อง</strong></td>
                <td><a target="_blank"
                       href="<?= Yii::$app->homeUrl ?>somebody-api/petition/<?= $session->get('id') ?>/2"
                       class="btn btn-3d btn-xs btn-white">ไปหน้าคำร้อง</a></td>
            </tr>
            </tbody>
        </table>
        <div class="text-center">
            <a href=" <?= Yii::$app->homeUrl ?>dummy-petition/insert-proposal-committee"
               class="btn btn-3d btn-dirtygreen">ส่งคำร้องแต่งตั้งกรรมการสอบ</a>
        </div>
    </div>
    <div class="padding-10">ข้อมูลเพิ่มเติม</div>
    <div class="slider-wrapper success-slider margin-bottom-30">
        <div id="slider"></div>
    </div>
    <div class="table-responsive text-center">
        <table id="table" class="table table-hover">
            <tbody>
            <tr class="additional-data">
                <td><strong>เวลาเริ่มการสอบ</strong></td>
                <td class="input">
                    <input name="project_name_th" type="text" class="form-control-sm time-start" readonly/>
                    <button class="btn btn-green btn-xs"><i class="fa fa-check nopadding-right"></i></button>
                </td>
                <td class="data">
                    <span></span>
                    <button class="btn btn-blue btn-xs"><i class="fa fa-pencil nopadding-right"></i></button>
                </td>
            </tr>
            <tr class="additional-data">
                <td><strong>เวลาสิ้นสุดการสอบ</strong></td>
                <td class="input">
                    <input name="project_name_eng" type="text" class="form-control-sm time-end" readonly/>
                    <button class="btn btn-green btn-xs"><i class="fa fa-check nopadding-right"></i></button>
                </td>
                <td class="data">
                    <span></span>
                    <button class="btn btn-blue btn-xs"><i class="fa fa-pencil nopadding-right"></i></button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-12">
        <button class="btn btn-lg btn-3d btn-green pull-right" id="next-step">ขั้นตอนต่อไป <i
                    class="fa fa-arrow-right"></i></button>
    </div>
    <div>
        <a href="<?= Yii::$app->homeUrl ?>dummy-petition/complete/1"
           class="btn btn-3d btn-red">ทำให้คำร้องขอสอบสำเร็จ</a>
        <a href="<?= Yii::$app->homeUrl ?>dummy-petition/complete/2"
           class="btn btn-3d btn-red">ทำให้คำร้องกรรมการสำเร็จ</a>
    </div>
</div>
<script>
    var process_body = $('.process-body');
    var process_id = '<?= $processStep->process_id ?>';
    var process_step_sequence = '<?= $processStep->process_step_sequence ?>';

    $(document).ready(function () {


        process_body.hide();
        var elm_no = '1';
        var pet_type = '1';
        var url = '<?= Yii::$app->homeUrl . 'somebody-api/petition/' . $session->get('id') ?>/' + pet_type;
        set_page_data(url, elm_no);
        elm_no = '2';
        pet_type = '2';
        url = '<?= Yii::$app->homeUrl . 'somebody-api/petition/' . $session->get('id') ?>/' + pet_type;
        set_page_data(url, elm_no);
        url = '<?= Yii::$app->homeUrl ?>project';
        set_additional_data(url);
    });

    $('.additional-data').find('td.input').find('button').on('click', function () {
        var url = '<?= Yii::$app->homeUrl ?>defend';
        var elm = $(this);
        do_additional_data(elm, url);
    });

    function set_page_data(url, elm_no) {
        $.getJSON(url, function (data) {
                var pet_id = $('#pet-id-' + elm_no);
                var pet_type = $('#pet-type-' + elm_no);
                var pet_status = $('#pet-status-' + elm_no);
                if (data[0] === null) {
                    pet_id.addClass('label label-primary');
                    pet_type.addClass('label label-primary');
                    pet_status.addClass('label label-primary');
                } else {
                    pet_id.html(data['petition_id']);
                    pet_type.html(data['petition_type']['petition_type_name']);
                    pet_status.html(data['petition_status']['petition_status_name']);
                    switch (data['petition_status']['petition_status_id']) {
                        case 0:
                            pet_status.addClass('label label-danger');
                            break;
                        case 1:
                            pet_status.addClass('label label-success');
                            break;
                        case 2:
                            pet_status.addClass('label label-info');
                            break;
                    }
                }
            }
        );
    }
</script>