<?php
Yii::$app->view->params['title'] = 'test';
?>
<script src="<?= Yii::$app->homeUrl ?>plugins/raphael-min.js"></script>
<script src="<?= Yii::$app->homeUrl ?>plugins/flowchart-latest.js"></script>

<textarea title="" id="test1" style="width: 100%" rows="10">
op1=>operation: My Operation|past
op2=>operation: Stuff|past
op3=>operation: Stuff|past
op4=>operation: Stuff|past

op1(right)->op2(right)->op3(right)->op4
</textarea>
<textarea title="" id="test2" style="width: 100%" rows="10">
op1=>operation: My Operation|past
op2=>operation: Stuff|past
op3=>operation: test|past

op1->op2
</textarea>
<div id="diagram1" style="width: 50%"></div>
<div id="diagram2"></div>
<script>
    var code1 = $('#test1').val();
    var code2 = $('#test2').val();

    $(document).ready(function () {
        draw(code1, 'diagram1');
        draw(code2, 'diagram2');
    });
    draw = function (code, id) {
        var diagram = flowchart.parse(code);
        diagram.drawSVG(id, {
            'x': 0,
            'y': 0,
            'line-width': 3,
            'line-length': 50,
            'text-margin': 10,
            'font-size': 14,
            'font-color': 'black',
            'line-color': 'black',
            'element-color': 'black',
            'fill': 'white',
            'yes-text': 'yes',
            'no-text': 'no',
            'arrow-end': 'block',
            'scale': 1,
            // style symbol types
            'symbols': {
                'start': {
//                'font-color': 'red',
//                'element-color': 'green',
//                'fill': 'yellow'
                },
                'end': {
//                'class': 'end-element'
                }
            },
            // even flowstate support ;-)
            'flowstate': {
                'past': {'fill': '#CCCCCC', 'font-size': 12},
                'current': {'fill': 'yellow', 'font-color': 'red', 'font-weight': 'bold'},
                'future': {'fill': '#FFFF99'},
                'request': {'fill': 'blue'},
                'invalid': {'fill': '#444444'},
                'approved': {'fill': '#58C4A3', 'font-size': 12, 'yes-text': 'APPROVED', 'no-text': 'n/a'},
                'rejected': {'fill': '#C45879', 'font-size': 12, 'yes-text': 'n/a', 'no-text': 'REJECTED'}
            }
        });
    }
</script>