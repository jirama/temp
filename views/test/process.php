<?php
Yii::$app->view->params['title'] = $process->processType->process_type_name;
$processSteps = $process->processSteps;
Yii::$app->view->params['processSteps'] = $processSteps;
?>

<div class="row process-wizard process-wizard-default">
    <?php foreach ($processSteps as $processStep): ?>
        <div class="col-xs-3 process-wizard-step complete">
            <div class="text-center process-wizard-stepnum">
                #<?= $processStep->process_step_sequence ?>
            </div>
            <div class="progress">
                <div class="progress-bar"></div>
            </div>
            <a href="<?= Yii::$app->homeUrl . 'test/process/' . $process->process_type_id . '/' . $processStep->process_step_sequence ?>"
               class="process-wizard-dot"></a>
            <div class="process-wizard-info text-center"><?= $processStep->process_step_name ?></div>
        </div>
    <?php endforeach; ?>
</div>

<div class="process-body panel panel-default">
    <div style="margin: 50px">
    <?php
    foreach ($processSteps as $processStep) {
        if ($processStep->process_step_sequence == $step) { ?>
            <?php Yii::$app->runAction('process/process-step', ['processId' => $processStep->process_id, 'processStepSequence' => $processStep->process_step_sequence]); ?>
            <?php
        }
    } ?>
    </div>
</div>