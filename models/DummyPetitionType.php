<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_petition_type".
 *
 * @property integer $petition_type_id
 * @property string $petition_type_name
 *
 * @property DummyPetition[] $dummyPetitions
 * @property DummyPetitionComponentTemplate[] $dummyPetitionComponentTemplates
 */
class DummyPetitionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_petition_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['petition_type_id', 'petition_type_name'], 'required'],
            [['petition_type_id'], 'integer'],
            [['petition_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'petition_type_id' => 'Petition Type ID',
            'petition_type_name' => 'Petition Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPetitions()
    {
        return $this->hasMany(DummyPetition::className(), ['petition_type_id' => 'petition_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPetitionComponentTemplates()
    {
        return $this->hasMany(DummyPetitionComponentTemplate::className(), ['petition_type_id' => 'petition_type_id']);
    }
}
