<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_petition".
 *
 * @property integer $petition_id
 * @property integer $petition_active
 * @property integer $petition_type_id
 * @property string $student_id
 * @property integer $petition_status_id
 *
 * @property DummyPetitionStatus $petitionStatus
 * @property DummyPerson $student
 * @property DummyPetitionType $petitionType
 * @property DummyPetitionComponent[] $dummyPetitionComponents
 */
class DummyPetition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_petition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['petition_active', 'petition_type_id', 'student_id', 'petition_status_id'], 'required'],
            [['petition_active', 'petition_type_id', 'petition_status_id'], 'integer'],
            [['student_id'], 'string', 'max' => 255],
            [['petition_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DummyPetitionStatus::className(), 'targetAttribute' => ['petition_status_id' => 'petition_status_id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => DummyPerson::className(), 'targetAttribute' => ['student_id' => 'person_id']],
            [['petition_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DummyPetitionType::className(), 'targetAttribute' => ['petition_type_id' => 'petition_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'petition_id' => 'Petition ID',
            'petition_active' => 'Petition Active',
            'petition_type_id' => 'Petition Type ID',
            'student_id' => 'Student ID',
            'petition_status_id' => 'Petition Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetitionStatus()
    {
        return $this->hasOne(DummyPetitionStatus::className(), ['petition_status_id' => 'petition_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(DummyPerson::className(), ['person_id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetitionType()
    {
        return $this->hasOne(DummyPetitionType::className(), ['petition_type_id' => 'petition_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPetitionComponents()
    {
        return $this->hasMany(DummyPetitionComponent::className(), ['petition_id' => 'petition_id']);
    }
}
