<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $project_id
 * @property string $student_id
 * @property string $project_name_th
 * @property string $project_name_eng
 *
 * @property Defend[] $defends
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'project_name_th', 'project_name_eng'], 'required'],
            [['student_id', 'project_name_th', 'project_name_eng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'student_id' => 'Student ID',
            'project_name_th' => 'Project Name Th',
            'project_name_eng' => 'Project Name Eng',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefends()
    {
        return $this->hasMany(Defend::className(), ['project_id' => 'project_id']);
    }
}
