<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_petition_status".
 *
 * @property integer $petition_status_id
 * @property string $petition_status_name
 *
 * @property DummyPetition[] $dummyPetitions
 */
class DummyPetitionStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_petition_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['petition_status_id', 'petition_status_name'], 'required'],
            [['petition_status_id'], 'integer'],
            [['petition_status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'petition_status_id' => 'Petition Status ID',
            'petition_status_name' => 'Petition Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPetitions()
    {
        return $this->hasMany(DummyPetition::className(), ['petition_status_id' => 'petition_status_id']);
    }
}
