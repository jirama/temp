<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester".
 *
 * @property integer $semester_id
 * @property integer $semester_no
 * @property integer $semester_year
 * @property integer $semester_active
 *
 * @property Advisor[] $advisors
 * @property Defend[] $defends
 */
class Semester extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'semester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['semester_no', 'semester_year', 'semester_active'], 'required'],
            [['semester_no', 'semester_year', 'semester_active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'semester_id' => 'Semester ID',
            'semester_no' => 'Semester No',
            'semester_year' => 'Semester Year',
            'semester_active' => 'Semester Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvisors()
    {
        return $this->hasMany(Advisor::className(), ['semester_id' => 'semester_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefends()
    {
        return $this->hasMany(Defend::className(), ['semester_id' => 'semester_id']);
    }
}
