<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_person_type".
 *
 * @property integer $person_type_id
 * @property string $person_type_name
 *
 * @property DummyPerson[] $dummyPeople
 */
class DummyPersonType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_person_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_type_id', 'person_type_name'], 'required'],
            [['person_type_id'], 'integer'],
            [['person_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPeople()
    {
        return $this->hasMany(DummyPerson::className(), ['person_type_id' => 'person_type_id']);
    }
}
