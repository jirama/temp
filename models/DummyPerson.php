<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_person".
 *
 * @property string $person_id
 * @property string $person_prefix
 * @property string $person_first_name
 * @property string $person_last_name
 * @property integer $person_type_id
 * @property integer $person_enroll_semester
 * @property integer $person_enroll_year
 *
 * @property DummyPersonType $personType
 * @property DummyPetition[] $dummyPetitions
 */
class DummyPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'person_prefix', 'person_first_name', 'person_last_name', 'person_type_id'], 'required'],
            [['person_type_id', 'person_enroll_semester', 'person_enroll_year'], 'integer'],
            [['person_id', 'person_prefix', 'person_first_name', 'person_last_name'], 'string', 'max' => 255],
            [['person_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DummyPersonType::className(), 'targetAttribute' => ['person_type_id' => 'person_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'person_prefix' => 'Person Prefix',
            'person_first_name' => 'Person First Name',
            'person_last_name' => 'Person Last Name',
            'person_type_id' => 'Person Type ID',
            'person_enroll_semester' => 'Person Enroll Semester',
            'person_enroll_year' => 'Person Enroll Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonType()
    {
        return $this->hasOne(DummyPersonType::className(), ['person_type_id' => 'person_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDummyPetitions()
    {
        return $this->hasMany(DummyPetition::className(), ['student_id' => 'person_id']);
    }
}
