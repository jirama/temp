<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_template".
 *
 * @property integer $process_step_template_id
 * @property integer $process_step_template_sequence
 * @property string $process_step_template_name
 * @property string $process_type_id
 *
 * @property ProcessType $processType
 * @property ProcessStepValidationRequired[] $processStepValidationRequireds
 */
class ProcessStepTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_template_id', 'process_step_template_sequence', 'process_step_template_name', 'process_type_id'], 'required'],
            [['process_step_template_id', 'process_step_template_sequence'], 'integer'],
            [['process_step_template_name', 'process_type_id'], 'string', 'max' => 255],
            [['process_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessType::className(), 'targetAttribute' => ['process_type_id' => 'process_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_template_id' => 'Process Step Template ID',
            'process_step_template_sequence' => 'Process Step Template Sequence',
            'process_step_template_name' => 'Process Step Template Name',
            'process_type_id' => 'Process Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessType()
    {
        return $this->hasOne(ProcessType::className(), ['process_type_id' => 'process_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepValidationRequireds()
    {
        return $this->hasMany(ProcessStepValidationRequired::className(), ['process_step_template_id' => 'process_step_template_id']);
    }
}
