<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_type".
 *
 * @property string $process_type_id
 * @property string $process_type_name
 *
 * @property Process[] $processes
 * @property ProcessStepTemplate[] $processStepTemplates
 */
class ProcessType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_type_id', 'process_type_name'], 'required'],
            [['process_type_id', 'process_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_type_id' => 'Process Type ID',
            'process_type_name' => 'Process Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcesses()
    {
        return $this->hasMany(Process::className(), ['process_type_id' => 'process_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepTemplates()
    {
        return $this->hasMany(ProcessStepTemplate::className(), ['process_type_id' => 'process_type_id']);
    }
}
