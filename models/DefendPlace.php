<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "defend_place".
 *
 * @property integer $defend_place_id
 * @property string $defend_place_name
 *
 * @property Defend[] $defends
 */
class DefendPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defend_place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['defend_place_id', 'defend_place_name'], 'required'],
            [['defend_place_id'], 'integer'],
            [['defend_place_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'defend_place_id' => 'Defend Place ID',
            'defend_place_name' => 'Defend Place Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefends()
    {
        return $this->hasMany(Defend::className(), ['defend_place_id' => 'defend_place_id']);
    }
}
