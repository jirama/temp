<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_validation".
 *
 * @property integer $process_step_validation_id
 * @property string $process_step_validation_name
 *
 * @property ProcessStepValidationRequired[] $processStepValidationRequireds
 */
class ProcessStepValidation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_validation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_validation_id', 'process_step_validation_name'], 'required'],
            [['process_step_validation_id'], 'integer'],
            [['process_step_validation_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_validation_id' => 'Process Step Validation ID',
            'process_step_validation_name' => 'Process Step Validation Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepValidationRequireds()
    {
        return $this->hasMany(ProcessStepValidationRequired::className(), ['process_step_validation_id' => 'process_step_validation_id']);
    }
}
