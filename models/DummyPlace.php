<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_place".
 *
 * @property integer $place_id
 * @property string $place_name
 */
class DummyPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id', 'place_name'], 'required'],
            [['place_id'], 'integer'],
            [['place_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'place_id' => 'Place ID',
            'place_name' => 'Place Name',
        ];
    }
}
