<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_action_require".
 *
 * @property integer $process_step_action_require_id
 * @property integer $process_step_template_id
 * @property integer $process_step_action_id
 *
 * @property ProcessStepAction $processStepAction
 * @property ProcessStepTemplate $processStepTemplate
 */
class ProcessStepActionRequire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_action_require';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_action_require_id', 'process_step_template_id', 'process_step_action_id'], 'required'],
            [['process_step_action_require_id', 'process_step_template_id', 'process_step_action_id'], 'integer'],
            [['process_step_action_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessStepAction::className(), 'targetAttribute' => ['process_step_action_id' => 'process_step_action_id']],
            [['process_step_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessStepTemplate::className(), 'targetAttribute' => ['process_step_template_id' => 'process_step_template_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_action_require_id' => 'Process Step Action Require ID',
            'process_step_template_id' => 'Process Step Template ID',
            'process_step_action_id' => 'Process Step Action ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepAction()
    {
        return $this->hasOne(ProcessStepAction::className(), ['process_step_action_id' => 'process_step_action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepTemplate()
    {
        return $this->hasOne(ProcessStepTemplate::className(), ['process_step_template_id' => 'process_step_template_id']);
    }
}
