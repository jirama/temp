<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy_petition_component".
 *
 * @property integer $petition_component_id
 * @property string $petition_component_name
 * @property string $petition_component_value
 * @property integer $petition_id
 *
 * @property DummyPetition $petition
 */
class DummyPetitionComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy_petition_component';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['petition_component_name', 'petition_id'], 'required'],
            [['petition_id'], 'integer'],
            [['petition_component_name', 'petition_component_value'], 'string', 'max' => 255],
            [['petition_id'], 'exist', 'skipOnError' => true, 'targetClass' => DummyPetition::className(), 'targetAttribute' => ['petition_id' => 'petition_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'petition_component_id' => 'Petition Component ID',
            'petition_component_name' => 'Petition Component Name',
            'petition_component_value' => 'Petition Component Value',
            'petition_id' => 'Petition ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetition()
    {
        return $this->hasOne(DummyPetition::className(), ['petition_id' => 'petition_id']);
    }
}
