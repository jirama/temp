<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advisor".
 *
 * @property integer $advisor_id
 * @property string $teacher_id
 * @property string $student_id
 * @property integer $advisor_position_id
 * @property integer $semester_id
 *
 * @property AdvisorPosition $advisorPosition
 * @property Semester $semester
 */
class Advisor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advisor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'student_id', 'advisor_position_id', 'semester_id'], 'required'],
            [['advisor_position_id', 'semester_id'], 'integer'],
            [['teacher_id', 'student_id'], 'string', 'max' => 255],
            [['advisor_position_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvisorPosition::className(), 'targetAttribute' => ['advisor_position_id' => 'advisor_position_id']],
            [['semester_id'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['semester_id' => 'semester_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'advisor_id' => 'Advisor ID',
            'teacher_id' => 'Teacher ID',
            'student_id' => 'Student ID',
            'advisor_position_id' => 'Advisor Position ID',
            'semester_id' => 'Semester ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvisorPosition()
    {
        return $this->hasOne(AdvisorPosition::className(), ['advisor_position_id' => 'advisor_position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['semester_id' => 'semester_id']);
    }
}
