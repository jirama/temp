<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advisor_position".
 *
 * @property integer $advisor_position_id
 * @property string $advisor_position_name
 *
 * @property Advisor[] $advisors
 */
class AdvisorPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advisor_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advisor_position_id', 'advisor_position_name'], 'required'],
            [['advisor_position_id'], 'integer'],
            [['advisor_position_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'advisor_position_id' => 'Advisor Position ID',
            'advisor_position_name' => 'Advisor Position Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvisors()
    {
        return $this->hasMany(Advisor::className(), ['position_advisor_id' => 'advisor_position_id']);
    }
}
