<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_status".
 *
 * @property integer $process_step_status_id
 * @property string $process_step_status_name
 *
 * @property ProcessStep[] $processSteps
 */
class ProcessStepStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_status_id', 'process_step_status_name'], 'required'],
            [['process_step_status_id'], 'integer'],
            [['process_step_status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_status_id' => 'Process Step Status ID',
            'process_step_status_name' => 'Process Step Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessSteps()
    {
        return $this->hasMany(ProcessStep::className(), ['process_step_status_id' => 'process_step_status_id']);
    }
}
