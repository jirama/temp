<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "defend".
 *
 * @property integer $defend_id
 * @property string $student_id
 * @property integer $defend_type_id
 * @property string $defend_date
 * @property string $defend_time_start
 * @property string $defend_time_end
 * @property integer $defend_place_id
 * @property integer $semester_id
 * @property integer $project_id
 * @property integer $defend_status_id
 *
 * @property Committee[] $committees
 * @property DefendPlace $defendPlace
 * @property DefendStatus $defendStatus
 * @property DefendType $defendType
 * @property Project $project
 * @property Semester $semester
 * @property DefendDoc $defendDoc
 */
class Defend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'defend_type_id', 'defend_date', 'defend_time_start', 'defend_time_end', 'defend_place_id', 'semester_id', 'project_id', 'defend_status_id'], 'required'],
            [['defend_type_id', 'defend_place_id', 'semester_id', 'project_id', 'defend_status_id'], 'integer'],
            [['student_id', 'defend_date', 'defend_time_start', 'defend_time_end'], 'string', 'max' => 255],
            [['defend_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => DefendPlace::className(), 'targetAttribute' => ['defend_place_id' => 'defend_place_id']],
            [['defend_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DefendStatus::className(), 'targetAttribute' => ['defend_status_id' => 'defend_status_id']],
            [['defend_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DefendType::className(), 'targetAttribute' => ['defend_type_id' => 'defend_type_id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'project_id']],
            [['semester_id'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['semester_id' => 'semester_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'defend_id' => 'Defend ID',
            'student_id' => 'Student ID',
            'defend_type_id' => 'Defend Type ID',
            'defend_date' => 'Defend Date',
            'defend_time_start' => 'Defend Time Start',
            'defend_time_end' => 'Defend Time End',
            'defend_place_id' => 'Defend Place ID',
            'semester_id' => 'Semester ID',
            'project_id' => 'Project ID',
            'defend_status_id' => 'Defend Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommittees()
    {
        return $this->hasMany(Committee::className(), ['defend_id' => 'defend_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefendPlace()
    {
        return $this->hasOne(DefendPlace::className(), ['defend_place_id' => 'defend_place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefendStatus()
    {
        return $this->hasOne(DefendStatus::className(), ['defend_status_id' => 'defend_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefendType()
    {
        return $this->hasOne(DefendType::className(), ['defend_type_id' => 'defend_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['semester_id' => 'semester_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefendDoc()
    {
        return $this->hasOne(DefendDoc::className(), ['defend_id' => 'defend_id']);
    }
}
