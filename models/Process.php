<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process".
 *
 * @property integer $process_id
 * @property string $student_id
 * @property string $process_type_id
 * @property integer $semester_id
 *
 * @property ProcessType $processType
 * @property Semester $semester
 * @property ProcessStep[] $processSteps
 */
class Process extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'process_type_id', 'semester_id'], 'required'],
            [['semester_id'], 'integer'],
            [['student_id', 'process_type_id'], 'string', 'max' => 255],
            [['process_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessType::className(), 'targetAttribute' => ['process_type_id' => 'process_type_id']],
            [['semester_id'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['semester_id' => 'semester_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_id' => 'Process ID',
            'student_id' => 'Student ID',
            'process_type_id' => 'Process Type ID',
            'semester_id' => 'Semester ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessType()
    {
        return $this->hasOne(ProcessType::className(), ['process_type_id' => 'process_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemester()
    {
        return $this->hasOne(Semester::className(), ['semester_id' => 'semester_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessSteps()
    {
        return $this->hasMany(ProcessStep::className(), ['process_id' => 'process_id']);
    }
}
