<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "defend_type".
 *
 * @property integer $defend_type_id
 * @property string $defend_type_name
 *
 * @property Defend[] $defends
 * @property DefendTypeDocRequire[] $defendTypeDocRequires
 */
class DefendType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defend_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['defend_type_id', 'defend_type_name'], 'required'],
            [['defend_type_id'], 'integer'],
            [['defend_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'defend_type_id' => 'Defend Type ID',
            'defend_type_name' => 'Defend Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefends()
    {
        return $this->hasMany(Defend::className(), ['defend_type_id' => 'defend_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefendTypeDocRequires()
    {
        return $this->hasMany(DefendTypeDocRequire::className(), ['defend_type_id' => 'defend_type_id']);
    }
}
