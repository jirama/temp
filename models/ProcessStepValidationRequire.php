<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_validation_require".
 *
 * @property integer $process_step_validation_require_id
 * @property integer $process_step_template_id
 * @property integer $process_step_validation_id
 *
 * @property ProcessStepTemplate $processStepTemplate
 * @property ProcessStepValidation $processStepValidation
 */
class ProcessStepValidationRequire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_validation_require';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_validation_require_id', 'process_step_template_id', 'process_step_validation_id'], 'required'],
            [['process_step_validation_require_id', 'process_step_template_id', 'process_step_validation_id'], 'integer'],
            [['process_step_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessStepTemplate::className(), 'targetAttribute' => ['process_step_template_id' => 'process_step_template_id']],
            [['process_step_validation_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessStepValidation::className(), 'targetAttribute' => ['process_step_validation_id' => 'process_step_validation_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_validation_require_id' => 'Process Step Validation Require ID',
            'process_step_template_id' => 'Process Step Template ID',
            'process_step_validation_id' => 'Process Step Validation ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepTemplate()
    {
        return $this->hasOne(ProcessStepTemplate::className(), ['process_step_template_id' => 'process_step_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepValidation()
    {
        return $this->hasOne(ProcessStepValidation::className(), ['process_step_validation_id' => 'process_step_validation_id']);
    }
}
