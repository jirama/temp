<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "defend_status".
 *
 * @property integer $defend_status_id
 * @property string $defend_status_name
 *
 * @property Defend[] $defends
 */
class DefendStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'defend_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['defend_status_id', 'defend_status_name'], 'required'],
            [['defend_status_id'], 'integer'],
            [['defend_status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'defend_status_id' => 'Defend Status ID',
            'defend_status_name' => 'Defend Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefends()
    {
        return $this->hasMany(Defend::className(), ['defend_status_id' => 'defend_status_id']);
    }
}
