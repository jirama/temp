<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step_action".
 *
 * @property integer $process_step_action_id
 * @property string $process_step_action_name
 *
 * @property ProcessStepActionRequire[] $processStepActionRequires
 */
class ProcessStepAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_action_id', 'process_step_action_name'], 'required'],
            [['process_step_action_id'], 'integer'],
            [['process_step_action_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_action_id' => 'Process Step Action ID',
            'process_step_action_name' => 'Process Step Action Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepActionRequires()
    {
        return $this->hasMany(ProcessStepActionRequire::className(), ['process_step_action_process_step_action_id' => 'process_step_action_id']);
    }
}
