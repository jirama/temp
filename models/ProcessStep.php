<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "process_step".
 *
 * @property integer $process_step_id
 * @property integer $process_step_sequence
 * @property string $process_step_name
 * @property integer $process_id
 * @property integer $process_step_status_id
 *
 * @property Process $process
 * @property ProcessStepStatus $processStepStatus
 */
class ProcessStep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'process_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_step_sequence', 'process_step_name', 'process_id', 'process_step_status_id'], 'required'],
            [['process_step_sequence', 'process_id', 'process_step_status_id'], 'integer'],
            [['process_step_name'], 'string', 'max' => 255],
            [['process_id'], 'exist', 'skipOnError' => true, 'targetClass' => Process::className(), 'targetAttribute' => ['process_id' => 'process_id']],
            [['process_step_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcessStepStatus::className(), 'targetAttribute' => ['process_step_status_id' => 'process_step_status_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'process_step_id' => 'Process Step ID',
            'process_step_sequence' => 'Process Step Sequence',
            'process_step_name' => 'Process Step Name',
            'process_id' => 'Process ID',
            'process_step_status_id' => 'Process Step Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcess()
    {
        return $this->hasOne(Process::className(), ['process_id' => 'process_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcessStepStatus()
    {
        return $this->hasOne(ProcessStepStatus::className(), ['process_step_status_id' => 'process_step_status_id']);
    }
}
